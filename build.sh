PUBLISH="FALSE"
INPUT=$(echo $1 | sed 's/.tex//g')
for arg in "$@"; do
  case $arg in
    --publish)
      PUBLISH="TRUE"
      # to be implemented
      shift
      ;;
    -v=*|--version=*)
      VERSION="${arg#*=}"
      # to be implemented
      shift
      ;;
    *)
      # unknown option
      ;;
  esac
done

if [[ -z $INPUT ]]; then
  echo "Please specify the input file."
  exit 1
fi

if [[ $PUBLISH = 'TRUE' ]]; then
  if [[ -z $VERSION ]]; then
    echo "Since you have enabled publish option you need to specify the desired version as well."
    exit 1
  fi
fi


pdflatex -halt-on-error $INPUT &> logs
test $? = 1 && cat logs | grep --color=always -B5 'Fatal error' && exit
bibtex $INPUT > /dev/null 2>&1
pdflatex $INPUT > /dev/null 2>&1
pdflatex $INPUT > /dev/null 2>&1
xdg-open $(basename ${INPUT} .tex).pdf > /dev/null 2>&1
rm -f *.toc
rm -f *.snm
rm -f *.nav
rm -f *.aux
rm -f *.out
rm -f *.gz
rm -f *.bbl
rm -f *.log
rm -f *.blg
rm -f *.spl
rm -f logs
